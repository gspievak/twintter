class LikeComment < ApplicationRecord
  belongs_to :comment
  belongs_to :user

  validate :already_liked?

  def already_liked?
    if post.users.include?(user)
      errors.add(:already_liked , "Você já deu like aqui")
    end
  end
end
