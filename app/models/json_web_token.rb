class JsonWebToken

    Secret = Rails.application.credentials.secret_key_base

    def self.encode(payload)
        JWT.encode(payload, Secret)
    end
    def self.decode(token)
        JWT.decode(token, Secret)
    end
end